class ApplicationMailer < ActionMailer::Base
  # default from: "noreply@example.com"
  default from: "james.maduka@zygone.com"
  layout 'mailer'
end
